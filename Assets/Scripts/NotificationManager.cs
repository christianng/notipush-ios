﻿using Firebase;
using Firebase.Auth;
using Firebase.Extensions;
using Firebase.Messaging;
using Proyecto26;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using UnityEngine.UI;

public class NotificationManager : MonoBehaviour
{
    public Text tokenText;
    public Text tokenText2;
    public Text message;
    public Text result;
    public InputField input;

    public static string idToken = "";

    public UnityEvent OnUserLogout;
    public UnityEvent OnUserLogin;

    private static NotificationManager instance = null;

    public static NotificationManager Instance { get { return instance; } }

    private static string currentTokenId = "";
    public string CurrentTokenId { get { return currentTokenId; } }

    private DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
    private FirebaseAuth auth;
    private Dictionary<string, FirebaseUser> userByAuth = new Dictionary<string, FirebaseUser>();
    private bool fetchingToken = false;

    private string registeredUserId;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
    }

    private void Start()
    {
        FirebaseApp.CheckAndFixDependenciesAsync()
            .ContinueWithOnMainThread(task =>
            {
                dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available)
                {
                    InitializeFirebase();
                }
                else { Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus); }
            });

        FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseMessaging.MessageReceived += OnMessageReceived;

        
    }

    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        auth = FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        auth.IdTokenChanged += IdTokenChanged;

        AuthStateChanged(this, null);
        Debug.Log("Done initalize Firebase");

        if(FirebaseAuth.DefaultInstance.CurrentUser == null)
        {
            Debug.Log("Signed in");
            StartCoroutine(SigninAnonymouslyAsync());
        } else
        {
            Debug.Log("YEs");
        }
    }

    private IEnumerator SigninAnonymouslyAsync()
    {
        Debug.Log("Attemping to sign anonymously...");
        var signedInAuth = auth.SignInAnonymouslyAsync();
        Debug.Log(FirebaseAuth.DefaultInstance.CurrentUser == null);

        signedInAuth
            .ContinueWith(task =>
            {
                if (task.IsFaulted) { Debug.LogError(task.Status + "  : aa"); }
                else if (task.IsCompleted)
                {
                    Debug.Log("Signed in anonymously successfully");
                    GetUserToken();
                }
            });

        yield return new WaitUntil(() => signedInAuth.IsCompleted);
    }

    private void IdTokenChanged(object sender, EventArgs e)
    {
        FirebaseAuth senderAuth = sender as FirebaseAuth;
        if (senderAuth == auth && senderAuth.CurrentUser != null && !fetchingToken)
        {
            senderAuth.CurrentUser.TokenAsync(true)
                .ContinueWithOnMainThread(task =>
                {
                    currentTokenId = task.Result.ToString();
                    Debug.Log("Current Token: " + task.Result);
                });

            //Debug.Log(String.Format("Token[0:8] = {0}", task.Result.Substring(0, 8)))
        }
    }

    private void AuthStateChanged(object sender, EventArgs eventArgs)
    {
        FirebaseAuth senderAuth = sender as FirebaseAuth;
        FirebaseUser user = null;
        if (senderAuth != null) userByAuth.TryGetValue(senderAuth.App.Name, out user);
        if (senderAuth == auth && senderAuth.CurrentUser != user)
        {
            bool signedIn = user != senderAuth.CurrentUser && senderAuth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.LogError("Signed out " + user.UserId);
                OnUserLogout.Invoke();
            }

            user = senderAuth.CurrentUser;
            userByAuth[senderAuth.App.Name] = user;
            if (signedIn)
            {
                ////Debug.Log("AuthStateChanged Signed in " + user.UserId);
                OnUserLogin.Invoke();
            }
        }
    }

    public void GetUserToken()
    {
        if (auth.CurrentUser == null)
        {
            Debug.LogError("Not signed in, unable to get token.");
            return;
        }
        Debug.Log("Fetching user token");
        fetchingToken = true;
        var tokenAsync = auth.CurrentUser.TokenAsync(true).ContinueWithOnMainThread(task =>
        {
            fetchingToken = false;
            currentTokenId = task.Result.ToString();
            Debug.Log("Successfully GetUserToken");
        });
    }

    void OnDestroy()
    {
        if (auth != null)
        {
            auth.StateChanged -= AuthStateChanged;
            auth.IdTokenChanged -= IdTokenChanged;
            auth = null;
            currentTokenId = "";
        }
    }

    public struct tokenData
    {
        public string tokenId;
    }

    [Serializable]
    public struct data
    {
        public string body;
    }

    public string to;

    [Serializable]
    public struct notification
    {
        public string title;
        public string text;
        public string body;
    }

    [Serializable]
    public class tired
    {
        //public Dictionary<string, string> data = new Dictionary<string, string>();
        public data data;
        public string to;
        //public Dictionary<string, string> notification = new Dictionary<string, string>();
        public notification notification;
    }

    private void Update()
    {
        tokenText2.text = currentTokenId;
        result.text = tokenText.text.Equals(tokenText2.text).ToString();
    }


    public void SendMsg()
    {
        tired a = new tired();

        data data = new data();
        data.body = "Send from Christian";

        notification noti = new notification();
        noti.title = "This is title";
        noti.text = "This is text";
        noti.body = "Christian sent you a message";

        a.data = data;
        a.notification = noti;
        a.to = input.text;

        //a.data.Add("body", "Title body");
        //a.to = input.text;
        //a.notification.Add("title", "This is the stupid title");
        //a.notification.Add("body", "This is the stupid body");
        //a.notification.Add("text", "This is the stupid text");

        ES3.Save("data", data);
        ES3.Save("to", input.text);
        ES3.Save("notification", noti);

        string b = "{\"data\":{\"body\":\"Send from Christian\"},\"to\":\"d9yxscIS-U-5uSNH5JeovJ:APA91bGZViC6yMkhrPGY57jk637VJ3ZhXx2km2YZcFfwX4832IG5YyfxAgqEsVHjENtlwqsCiJGd-_S3CINlGDDptQmyMBeTSeAH57VsS46XmFMYHMi8FK-KiqDsKu0kcVIogIh1xJE5\",\"notification\":{\"title\":\"This is title\",\"text\":\"This is text\",\"body\":\"This is body\"}}";
        Debug.Log(b);

        //RestClient.DefaultRequestHeaders["Authorization"] = "Bearer " + idToken;
        RestClient.DefaultRequestHeaders["Authorization"] = "Bearer AAAAiSlmyKE:APA91bFms8pWuGq3e14IrZQxQMvwkWqabcqtRBdSY4-07DE-_piIexPfv75VqDp6DlagnVszy9vZ6RS1pxp95AVeJI54TBPq_XdDRBz4TFXES4fXtmiw2OXceSJX2AlUavBq0oRWus60";
        RestClient.Post("https://fcm.googleapis.com/fcm/send", a)
            .Then(response => result.text = response.Text)
            .Then(response => { RestClient.ClearDefaultHeaders(); })
            .Catch(error => result.text = error.Message);
    }

    private void OnTokenReceived(object sender, TokenReceivedEventArgs token)
    {
        Debug.Log("Received registration token: " + token.Token);
        FirebaseMessaging.SubscribeAsync("/topics/all");
        tokenText.text = token.Token;
        tokenData a = new tokenData();
        a.tokenId = token.Token;
        RestClient.Post("https://notipushtestios.firebaseio.com/123.json", a);

        idToken = token.Token;
    }

    private void OnMessageReceived(object sender, MessageReceivedEventArgs e)
    {
        Debug.Log("Received a new message from: " + e.Message.From);
        message.text = "Received new message " + e.Message.Data["body"];

        string roomName = e.Message.Data["roomName"];

        if(!PhotonNetwork.InRoom) NetworkManager.instance.CreateRoom(roomName);
        //message.text = e.Message.Notification.Title.ToString();
    }
}
