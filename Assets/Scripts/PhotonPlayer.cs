﻿using Boo.Lang.Environments;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PhotonPlayer : MonoBehaviour
{
    private PhotonView PV;
    public GameObject myAvatar;

    private void Start()
    {
        PV = GetComponent<PhotonView>();

        if (PV.IsMine)
        {
            int length = SpawnPosition.instace.spawnPoints.Length;
            int spawnIndex = Random.Range(0, length);
            myAvatar = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerAvatar"),
                SpawnPosition.instace.spawnPoints[spawnIndex].position, SpawnPosition.instace.spawnPoints[spawnIndex].rotation, 0);
        }
    }
}
