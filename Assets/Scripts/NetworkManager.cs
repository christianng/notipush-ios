﻿using Photon.Pun;
using Photon.Realtime;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static NetworkManager instance;

    private void Awake()
    {
        if (NetworkManager.instance == null) NetworkManager.instance = this;
        else
        {
            if(NetworkManager.instance != this)
            {
                Destroy(NetworkManager.instance.gameObject);
                NetworkManager.instance = this;
            }
        }

        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        PhotonNetwork.AuthValues = new AuthenticationValues("1");
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Player has connected to the Photon master server");
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public void CreateRoom()
    {
        Debug.Log("Trying to create a new room.");
        RoomOptions roomOps = new RoomOptions() { IsVisible = false, IsOpen = true, MaxPlayers = (byte)2, PublishUserId = true };
        PhotonNetwork.JoinOrCreateRoom("Room", roomOps, null, new string[] { "4" });
    }

    public void CreateRoom(string roomName)
    {
        Debug.Log("Trying to create a new room.");
        RoomOptions roomOps = new RoomOptions() { IsVisible = false, IsOpen = true, MaxPlayers = (byte)2, PublishUserId = true };
        PhotonNetwork.JoinOrCreateRoom(roomName, roomOps, null, new string[] { "3" });
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }

    private void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if(scene.buildIndex == 1)
        {
            RPC_CreatePlayer();
        }
    }

    [PunRPC]
    private void RPC_CreatePlayer()
    {
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonNetworkPlayer"), transform.position, transform.rotation, 0);
    }

    private void StartGame()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        PhotonNetwork.LoadLevel(1);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("We are noew in a room");
        StartGame();
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("Joined failed: " + message);
        CreateRoom();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a new room but failed: " + message);
        CreateRoom();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("A new user has joined the room.");
    }
}
