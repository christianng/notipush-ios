﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPosition : MonoBehaviour
{
    public static SpawnPosition instace;
    public Transform[] spawnPoints;

    private void OnEnable()
    {
        if (SpawnPosition.instace == null)
        {
            SpawnPosition.instace = this;
        }
    }
}
